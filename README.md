# NLControl Graphic User Interface

### Installation:

1) Download NLControl-gui files and copy them somewhere to your local computer.

2) Install Wolfram Engine (3.9 GB)

    a) Goto https://www.wolfram.com/developer/ and press red download icon.

    b) The web browser will be redirected to the next page, press button "get your license"

    c) Create Wolfram ID or sign in, if your already have Wolfram ID

    d) Run the Engine installer file.

    e) Open command window, type: `wolframscript -activate`

3) Install Python (75 MB) https://www.python.org/downloads/

4) Copy NLControl package files https://gitlab.com/maristonso/nlcontrol into a directory, where Wolfram Engine can find them. For Windows, the typical catalog for application pacakges is C:\Program Files\Wolfram Research\Wolfram Engine\12.0\AddOns\Applications.

5) Check if javascript is enabled in your web browser.

### Running:
1) Run python script `main.py`. For that open command prompt and type `py main.py`.

2) Open web browser and got location http://localhost:8000/

Alternatively, Windows users may run `NLControl.bat` file, which executes both steps 1) and 2)


### Possible Obstacles:

1) Internet connection is necessary for using NLControl GUI.


2) After some weeks or months, Wolfram Engine may ask for re-activation. It stops working and writes a message: 

`Run wolframscript -activate to activate it.`

It it happens, then do so, i.e. open command prompt and run:
`wolframscript -activate`
