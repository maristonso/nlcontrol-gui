let cIoEq1 = {
    "systemEqs" : "y''[t] == u[t]y'[t] + u'[t]y[t]",
    "inputVars" : "u[t]",
    "outputVars" : "y[t]",
    "timeVar" : "t"
};

let cIoEq2 = {
    "systemEqs" : "y'''[t] == u[t]y'[t] + u''[t]y[t]",
    "inputVars" : "u[t]",
    "outputVars" : "y[t]",
    "timeVar" : "t"
};

let cIoEq3 = {
    "systemEqs" : "{y1''[t] == u1[t]y2'[t]+u2''[t], y2''[t] == u2'[t] y2[t]+u1''[t]}",
    "inputVars" : "{u1[t], u2[t]}",
    "outputVars" : "{y1[t], y2[t]}",
    "timeVar" : "t"
}

let cIoEq4 = {
    "systemEqs" : "{y1''[t] == u1[t] y3'[t]+u2''[t] +u3[t], y2'[t] == u2'[t] y2[t]+u1''[t], y3'[t]==u1[t]+u2[t]}",
    "inputVars" : "{u1[t], u2[t], u3[t]}",
    "outputVars" : "{y1[t], y2[t], y3[t]}",
    "timeVar" : "t"
}

let cSsEq1 = {
    "systemEqs" : "{x1'[t] == x2[t]^2+u[t], x2'[t] == x1[t]+x2[t]}",
    "inputVars" : "u[t]",
    "timeVar" : "t" 
}

let cSsEq2 = {
    "systemEqs" : "{x1'[t] == x2[t]^2+u1[t], x2'[t] == x1[t]+x2[t], x3'[t] == u2[t]}",
    "inputVars" : "{u1[t], u2[t]}",
    "timeVar" : "t" 
}

let cSsEqLin8 = {
    "systemEqs" : "Sta"
}

let dIoEq1 = {
    "systemEqs" : "y[t+3] == u[t]y[t+1]+u[t+2]",
    "inputVars" : "u[t]",
    "outputVars" : "y[t]",
    "timeVar" : "t"
}

let dIoEq2 = {
    "systemEqs" : "{y1[t+3] == u1[t]y2[t+1]+u2[t+2], y2[t+2] == u2[t] y2[t]+u1[t+1]}",
    "inputVars" : "{u1[t], u2[t]}",
    "outputVars" : "{y1[t], y2[t]}",
    "timeVar" : "t"
}

let dSsEq1 = {
    "systemEqs" : "{x1[t+1] == x2[t]^2+u[t], x2[t+1] == x1[t]+x2[t]}",
    "inputVars" : "u[t]",
    "timeVar" : "t" 
}

let dSsEq2 = {
    "systemEqs" : "{x1[t+1] == x2[t]+u[t], x2[t+1] == x1[t]+x2[t]u[t],x3[t+1]==u[t]}",
    "inputVars" : "u[t]",
    "timeVar" : "t" 
}

let dSsEq3 = {
    "systemEqs" : "{x1[t+1] == u1[t], x2[t+1] == x3[t]u1[t], x3[t+1] == x4[t]u1[t], x4[t+1] == x5[t]u1[t], x5[t+1] == x6[t] u1[t], x6[t+1] == u2[t]}",
    "inputVars" : "{u1[t], u2[t]}",
    "timeVar" : "t" 
}

let tIoEq1 = {
    "systemEqs" : "DeltaDerivative[2][y][t] == u[t]DeltaDerivative[1][y][t] + DeltaDerivative[1][u][t]y[t]",
    "inputVars" : "u[t]",
    "outputVars" : "y[t]",
    "timeVar" : "t"
}

let tIoEq2 = {
    "systemEqs" : "DeltaDerivative[3][y][t] == u[t] DeltaDerivative[1][y][t] + DeltaDerivative[2][u][t] y[t]",
    "inputVars" : "u[t]",
    "outputVars" : "y[t]",
    "timeVar" : "t"
};

let tIoEq3 = {
    "systemEqs" : "{DeltaDerivative[2][y1][t] == u1[t] DeltaDerivative[1][y2][t]+ DeltaDerivative[2][u2][t], DeltaDerivative[2][y2][t] == DeltaDerivative[1][u2][t] y2[t] + DeltaDerivative[2][u1][t]}",
    "inputVars" : "{u1[t], u2[t]}",
    "outputVars" : "{y1[t], y2[t]}",
    "timeVar" : "t"
}

let tIoEq4 = {
    "systemEqs" : "{DeltaDerivative[2][y1][t] == u1[t] DeltaDerivative[1][y3][t]+ DeltaDerivative[2][u2][t] +u3[t], DeltaDerivative[1][y2][t] == DeltaDerivative[1][u2][t] y2[t] + DeltaDerivative[2][u1][t], DeltaDerivative[1][y3][t]==u1[t]+u2[t]}",
    "inputVars" : "{u1[t], u2[t], u3[t]}",
    "outputVars" : "{y1[t], y2[t], y3[t]}",
    "timeVar" : "t"
}

let tSsEq1 = {
    "systemEqs" : "{DeltaDerivative[1][x1][t] == x2[t]^2+u[t], DeltaDerivative[1][x2][t] == x1[t]+x2[t]}",
    "inputVars" : "u[t]",
    "timeVar" : "t" 
}

let tSsEq2 = {
    "systemEqs" : "{DeltaDerivative[1][x1][t] == x2[t]^2+u1[t], DeltaDerivative[1][x2][t] == x1[t]+x2[t], DeltaDerivative[1][x3][t] == u2[t]}",
    "inputVars" : "{u1[t], u2[t]}",
    "timeVar" : "t" 
}

let mathExamples = {
    "TimeDerivative" : {
        "IO" : {
            "FromIOToOreP": {
                "Example1" : { 
                    "polynomialVar": "s",
                    ... cIoEq1 
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... cIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... cIoEq3
                }
            },
            "ModelMatching" : {
                "Example1" : {
                    "referenceModel" : "y''[t] == v[t]",
                    "referenceVar": "v[t]",
                    ... cIoEq1 
                },
                "Example2" : {
                    "referenceModel" : "y''[t] == v[t]",
                    "referenceVar": "v[t]",
                    ... cIoEq2
                },
            },
            "OrePolynomials": {
                "Example1" : { 
                    "polynomialVar": "s",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    ... cIoEq1 
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... cIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... cIoEq3
                }
            },
            "OreRationals" : {
                "Example1" :{
                    "polynomialVar": "s",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    "polyq1": "OreP[1,0]",
                    "polyq2": "OreP[1, u[t]]",
                    ... cIoEq1  
                }
            },
            "Realization" :  {
                "Example1" : {
                    "stateVars" : "x",
                    ... cIoEq1
                },
                "Example2" : {
                    "stateVars" : "x",
                    ... cIoEq2
                },
                "Example3" : {
                    "stateVars" : "x",
                    ... cIoEq3
                }
            },
            "Reduction" :  {
                "Example1" : cIoEq1,
                "Example2" : cIoEq2,
                "Example3" : cIoEq3
            },
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... cIoEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... cIoEq2
                }
            },    
            "TransferFunction" : {
                "Example1" : {
                    "polynomialVar": "s",
                    ... cIoEq1
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... cIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... cIoEq3
                }
            }   
        },
        "SS" : {
            "Accessibility" : {
                "Example1" : cSsEq1,
                "Example2" : cSsEq2
            },
            "Linearization" :  {
                "Example1" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... cSsEq1
                },
                "Example2" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... cSsEq2
                }
            },
            "Identifiability" : {
                "Example1" : {
                    "systemEqs" : "{x1'[t] == a1 x2[t] + a2, x2'[t] == x2[t] + u[t]}",
                    "inputVars" : "u[t]",
                    "timeVar" : "t",
                    "parameters" : "{a1, a2}"
                },  
            }, 
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... cSsEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... cSsEq2
                }
            },
            "Observability" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... cSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... cSsEq2
                }
            },
            "ObserverForm" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... cSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... cSsEq2
                }
            },  
            "StateSpaceToIO" :  {
                "Example1" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... cSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... cSsEq2
                }
            },    
            "TransferFunction" :  {
                "Example1" : {
                    "polynomialVar": "s",
                    "outputEqs" : "y[t] == x1[t]",
                    ... cSsEq1
                }
            }
        }
    },
    "Shift" : {
        "IO" : {  
            "FromIOToOreP" : {
                "Example1" : {
                    "polynomialVar" : "z",
                    ... dIoEq1
                },
                "Example2" : {
                    "polynomialVar": "z",
                    ... dIoEq2
                }
            },
            "ModelMatching" : {
                "Example1" : {
                    "referenceModel" : "y[t+2] == v[t]",
                    "referenceVar": "v[t]",
                    ... dIoEq1 
                },
                "Example2" : {
                    "referenceModel" : "y[t+2] == v[t]",
                    "referenceVar": "v[t]",
                    ... dIoEq2
                },
            },
            "OrePolynomials": {
                "Example1" : { 
                    "polynomialVar": "z",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    ... dIoEq1 
                }
            },
            "OreRationals" : {
                "Example1" :{
                    "polynomialVar": "z",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    "polyq1": "OreP[1,0]",
                    "polyq2": "OreP[1, u[t]]",
                    ... dIoEq1  
                }
            },
            "Realization": {
                "Example1" : {
                    "stateVars": "x",
                    ... dIoEq1
                },
                "Example2" : {
                    "stateVars": "x",
                    ... dIoEq2
                }
            },
            "Reduction" :  {
                "Example1" : dIoEq1,
                "Example2" : dIoEq2
            },
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... dIoEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... dIoEq2
                }
            },    
            "TransferFunction" :  {
                "Example1" : {
                    "polynomialVar": "z",
                    ... dIoEq1
                }
            }   
        },
        "SS" : {
            "Accessibility" : {
                "Example1" : dSsEq1,
                "Example2" : dSsEq2
            },
            "Linearization" :  {
                "Example1" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... dSsEq1
                },
                "Example2" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... dSsEq2
                },
                "Example3" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... dSsEq3
                }
            },
            "Identifiability" : {
                "Example1" : {
                    "systemEqs" : "{x1[t+1] == a1 x2[t] + a2, x2[t+2] == x2[t] + u[t]}",
                    "inputVars" : "u[t]",
                    "timeVar" : "t",
                    "parameters" : "{a1, a2}"
                },  
            },
            "Observability" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... dSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... dSsEq2
                }
            },
            "ObserverForm" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... dSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... dSsEq2
                }
            },
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... dSsEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... dSsEq2
                }
            },
            "StateSpaceToIO" :  {
                "Example1" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... dSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... dSsEq2
                }
            },
            "Submersivity" :  {
                "Example1" : dSsEq1,
                "Example2" : dSsEq2
            },     
            "TransferFunction" :  {
                "Example1" : {
                    "polynomialVar": "z",
                    "outputEqs": "y[t] == x1[t]",
                    ... dSsEq1
                }
            },
            
        }   
    },
    "TimeScale" : {
        "IO" : {
            "FromIOToOreP": {
                "Example1" : { 
                    "polynomialVar": "s",
                    ... tIoEq1 
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... tIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... tIoEq3
                }
            },
            "ModelMatching" : {
                "Example1" : {
                    "referenceModel" : "DeltaDerivative[2][y][t] == v[t]",
                    "referenceVar": "v[t]",
                    ... tIoEq1 
                },
                "Example2" : {
                    "referenceModel" : "DeltaDerivative[2][y][t] == v[t]",
                    "referenceVar": "v[t]",
                    ... tIoEq2
                },
            },
            "OrePolynomials": {
                "Example1" : { 
                    "polynomialVar": "s",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    ... tIoEq1 
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... tIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... tIoEq3
                }
            },
            "OreRationals" : {
                "Example1" :{
                    "polynomialVar": "s",
                    "poly1": "OreP[1,0,y[t]]",
                    "poly2": "OreP[u[t],0]",
                    "polyq1": "OreP[1,0]",
                    "polyq2": "OreP[1, u[t]]",
                    ... tIoEq1  
                }
            },
            "Realization" :  {
                "Example1" : {
                    "stateVars" : "x",
                    ... tIoEq1
                },
                "Example2" : {
                    "stateVars" : "x",
                    ... tIoEq2
                },
                "Example3" : {
                    "stateVars" : "x",
                    ... tIoEq3
                }
            },
            "Reduction" :  {
                "Example1" : tIoEq1,
                "Example2" : tIoEq2,
                "Example3" : tIoEq3
            },
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... tIoEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... tIoEq2
                }
            },    
            "TransferFunction" : {
                "Example1" : {
                    "polynomialVar": "s",
                    ... tIoEq1
                },
                "Example2" : {
                    "polynomialVar": "s",
                    ... tIoEq2
                },
                "Example3" : {
                    "polynomialVar": "s",
                    ... tIoEq3
                }
            }   
        },
        "SS" : {
            "Accessibility" : {
                "Example1" : tSsEq1,
                "Example2" : tSsEq2
            },
            "Linearization" :  {
                "Example1" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... tSsEq1
                },
                "Example2" : {
                    "newStateVars" : "z",
                    "newInputVars" : "v",
                    ... tSsEq2
                }
            },
            "Identifiability" : {
                "Example1" : {
                    "systemEqs" : "{DeltaDerivative[1][x1][t] == a1 x2[t] + a2, DeltaDerivative[1][x2][t] == x2[t] + u[t]}",
                    "inputVars" : "u[t]",
                    "timeVar" : "t",
                    "parameters" : "{a1, a2}"
                },  
            }, 
            "SequenceH" :  {
                "Example1" : {
                    "subspaceNumber" : "All",
                    ... tSsEq1
                },
                "Example2" : {
                    "subspaceNumber" : "All",
                    ... tSsEq2
                }
            },
            "Observability" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... tSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... tSsEq2
                }
            },
            "ObserverForm" :  {
                "Example1" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... tSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t] == x1[t]",
                    ... tSsEq2
                }
            },  
            "StateSpaceToIO" :  {
                "Example1" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... tSsEq1
                },
                "Example2" : {
                    "outputEqs" : "y[t]==x1[t]",
                    ... tSsEq2
                }
            },    
            "TransferFunction" :  {
                "Example1" : {
                    "polynomialVar": "s",
                    "outputEqs" : "y[t] == x1[t]",
                    ... tSsEq1
                }
            }
        }
    }
}