 cIoEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\ddot{y} & = & y \\dot{u}+u \\dot{y} \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'outputVars' : '$$y(t)$$ ',
    'timeVar' : '$$\\text{t}$$ '
};

 cIoEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  y^{(3)} & = & y \\ddot{u}+u \\dot{y} \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'outputVars' : '$$y(t)$$ ',
    'timeVar' : '$$\\text{t}$$ '
};

 cIoEq3 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\ddot{y}_1 & = & u_1 \\dot{y}_2+\\ddot{u}_2 \\\\  \\ddot{y}_2 & = & y_2 \\dot{u}_2+\\ddot{u}_1 \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'outputVars' : '$$\\left\\{y_1,y_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 cIoEq4 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\ddot{y}_1 & = & u_1 \\dot{y}_3+\\ddot{u}_2+u_3 \\\\  \\dot{y}_2 & = & y_2 \\dot{u}_2+\\ddot{u}_1 \\\\  \\dot{y}_3 & = & u_1+u_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2,u_3\\right\\}$$ ',
    'outputVars' : '$$\\left\\{y_1,y_2,y_3\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 cSsEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\dot{x}_1 & = & u+x_2{}^2 \\\\  \\dot{x}_2 & = & x_1+x_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 cSsEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\dot{x}_1 & = & u_1+x_2{}^2 \\\\  \\dot{x}_2 & = & x_1+x_2 \\\\  \\dot{x}_3 & = & u_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 cSsEqLin8 = {
    'systemEqs' : 'Sta'
}

 dIoEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  y(t+3) & = & u(t) y(t+1)+u(t+2) \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'outputVars' : '$$y(t)$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 dIoEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  y_1(t+3) & = & u_1(t) y_2(t+1)+u_2(t+2) \\\\  y_2(t+2) & = & u_2(t) y_2(t)+u_1(t+1) \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'outputVars' : '$$\\left\\{y_1,y_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 dSsEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  x_1(t+1) & = & u(t)+x_2(t){}^2 \\\\  x_2(t+1) & = & x_1(t)+x_2(t) \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 dSsEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  x_1(t+1) & = & u(t)+x_2(t) \\\\  x_2(t+1) & = & u(t) x_2(t)+x_1(t) \\\\  x_3(t+1) & = & u(t) \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 dSsEq3 = {
    'systemEqs' : '$$\\begin{array}{rcl}  x_1(t+1) & = & u_1(t) \\\\  x_2(t+1) & = & u_1(t) x_3(t) \\\\  x_3(t+1) & = & u_1(t) x_4(t) \\\\  x_4(t+1) & = & u_1(t) x_5(t) \\\\  x_5(t+1) & = & u_1(t) x_6(t) \\\\  x_6(t+1) & = & u_2(t) \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 tIoEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  y^{\\langle 2\\rangle } & = & y u^{\\langle 1\\rangle }+u y^{\\langle 1\\rangle } \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'outputVars' : '$$y(t)$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 tIoEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  y^{\\langle 3\\rangle } & = & y u^{\\langle 2\\rangle }+u y^{\\langle 1\\rangle } \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'outputVars' : '$$y(t)$$ ',
    'timeVar' : '$$\\text{t}$$ '
};

 tIoEq3 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\text{y1}^{\\langle 2\\rangle } & = & u_1 y_2{}^{\\langle 1\\rangle }+\\text{u2}^{\\langle 2\\rangle } \\\\  y_2{}^{\\langle 2\\rangle } & = & u_1{}^{\\langle 2\\rangle }+y_2 \\text{u2}^{\\langle 1\\rangle } \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'outputVars' : '$$\\left\\{y_1,y_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 tIoEq4 = {
    'systemEqs' : '$$\\begin{array}{rcl}  \\text{y1}^{\\langle 2\\rangle } & = & u_1 \\text{y3}^{\\langle 1\\rangle }+u_2{}^{\\langle 2\\rangle }+u_3 \\\\  y_2{}^{\\langle 1\\rangle } & = & y_2 u_2{}^{\\langle 1\\rangle }+u_1{}^{\\langle 2\\rangle } \\\\  \\text{y3}^{\\langle 1\\rangle } & = & u_1+u_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2,u_3\\right\\}$$ ',
    'outputVars' : '$$\\left\\{y_1,y_2,y_3\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ '
}

 tSsEq1 = {
    'systemEqs' : '$$\\begin{array}{rcl}  x_1{}^{\\langle 1\\rangle } & = & u+x_2{}^2 \\\\  x_2{}^{\\langle 1\\rangle } & = & x_1+x_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$u(t)$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 tSsEq2 = {
    'systemEqs' : '$$\\begin{array}{rcl}  x_1{}^{\\langle 1\\rangle } & = & u_1+x_2{}^2 \\\\  x_2{}^{\\langle 1\\rangle } & = & x_1+x_2 \\\\  \\text{x3}^{\\langle 1\\rangle } & = & u_2 \\\\ \\end{array}$$ ',
    'inputVars' : '$$\\left\\{u_1,u_2\\right\\}$$ ',
    'timeVar' : '$$\\text{t}$$ ' 
}

 let texExamples = {
    'TimeDerivative' : {
        'IO' : {
            'FromIOToOreP': {
                'Example1' : { 
                    'polynomialVar': 's',
                    ... cIoEq1 
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... cIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... cIoEq3
                }
            },
            'ModelMatching' : {
                'Example1' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  \\ddot{y} & = & v \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... cIoEq1 
                },
                'Example2' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  \\ddot{y} & = & v \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... cIoEq2
                },
            },
            'OrePolynomials': {
                'Example1' : { 
                    'polynomialVar': 's',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    ... cIoEq1 
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... cIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... cIoEq3
                }
            },
            'OreRationals' : {
                'Example1' :{
                    'polynomialVar': 's',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    'polyq1': 'OreP[1,0]',
                    'polyq2': '$$\\text{OreP}(1,u)$$ ',
                    ... cIoEq1  
                }
            },
            'Realization' :  {
                'Example1' : {
                    'stateVars' : 'x',
                    ... cIoEq1
                },
                'Example2' : {
                    'stateVars' : 'x',
                    ... cIoEq2
                },
                'Example3' : {
                    'stateVars' : 'x',
                    ... cIoEq3
                }
            },
            'Reduction' :  {
                'Example1' : cIoEq1,
                'Example2' : cIoEq2,
                'Example3' : cIoEq3
            },
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... cIoEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... cIoEq2
                }
            },    
            'TransferFunction' : {
                'Example1' : {
                    'polynomialVar': 's',
                    ... cIoEq1
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... cIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... cIoEq3
                }
            }   
        },
        'SS' : {
            'Accessibility' : {
                'Example1' : cSsEq1,
                'Example2' : cSsEq2
            },
            'Linearization' :  {
                'Example1' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... cSsEq1
                },
                'Example2' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... cSsEq2
                }
            },
            'Identifiability' : {
                'Example1' : {
                    'systemEqs' : '$$\\begin{array}{rcl}  \\dot{x}_1 & = & \\text{a1} x_2+\\text{a2} \\\\  \\dot{x}_2 & = & u+x_2 \\\\ \\end{array}$$ ',
                    'inputVars' : '$$u(t)$$ ',
                    'timeVar' : '$$\\text{t}$$ ',
                    'parameters' : '{a1, a2}'
                },  
            }, 
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... cSsEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... cSsEq2
                }
            },
            'Observability' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq2
                }
            },
            'ObserverForm' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq2
                }
            },  
            'StateSpaceToIO' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq2
                }
            },    
            'TransferFunction' :  {
                'Example1' : {
                    'polynomialVar': 's',
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... cSsEq1
                }
            }
        }
    },
    'Shift' : {
        'IO' : {  
            'FromIOToOreP' : {
                'Example1' : {
                    'polynomialVar' : 'z',
                    ... dIoEq1
                },
                'Example2' : {
                    'polynomialVar': 'z',
                    ... dIoEq2
                }
            },
            'ModelMatching' : {
                'Example1' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  y(t+2) & = & v(t) \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... dIoEq1 
                },
                'Example2' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  y(t+2) & = & v(t) \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... dIoEq2
                },
            },
            'OrePolynomials': {
                'Example1' : { 
                    'polynomialVar': 'z',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    ... dIoEq1 
                }
            },
            'OreRationals' : {
                'Example1' :{
                    'polynomialVar': 'z',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    'polyq1': 'OreP[1,0]',
                    'polyq2': '$$\\text{OreP}(1,u)$$ ',
                    ... dIoEq1  
                }
            },
            'Realization': {
                'Example1' : {
                    'stateVars': 'x',
                    ... dIoEq1
                },
                'Example2' : {
                    'stateVars': 'x',
                    ... dIoEq2
                }
            },
            'Reduction' :  {
                'Example1' : dIoEq1,
                'Example2' : dIoEq2
            },
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... dIoEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... dIoEq2
                }
            },    
            'TransferFunction' :  {
                'Example1' : {
                    'polynomialVar': 'z',
                    ... dIoEq1
                }
            }   
        },
        'SS' : {
            'Accessibility' : {
                'Example1' : dSsEq1,
                'Example2' : dSsEq2
            },
            'Linearization' :  {
                'Example1' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... dSsEq1
                },
                'Example2' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... dSsEq2
                },
                'Example3' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... dSsEq3
                }
            },
            'Identifiability' : {
                'Example1' : {
                    'systemEqs' : '$$\\begin{array}{rcl}  x_1(t+1) & = & \\text{a1} x_2(t)+\\text{a2} \\\\  x_2(t+2) & = & u(t)+x_2(t) \\\\ \\end{array}$$ ',
                    'inputVars' : '$$u(t)$$ ',
                    'timeVar' : '$$\\text{t}$$ ',
                    'parameters' : '{a1, a2}'
                },  
            },
            'Observability' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq2
                }
            },
            'ObserverForm' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq2
                }
            },
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... dSsEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... dSsEq2
                }
            },
            'StateSpaceToIO' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq2
                }
            },
            'Submersivity' :  {
                'Example1' : dSsEq1,
                'Example2' : dSsEq2
            },     
            'TransferFunction' :  {
                'Example1' : {
                    'polynomialVar': 'z',
                    'outputEqs': '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... dSsEq1
                }
            },
            
        }   
    },
    'TimeScale' : {
        'IO' : {
            'FromIOToOreP': {
                'Example1' : { 
                    'polynomialVar': 's',
                    ... tIoEq1 
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... tIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... tIoEq3
                }
            },
            'ModelMatching' : {
                'Example1' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  y^{\\langle 2\\rangle } & = & v \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... tIoEq1 
                },
                'Example2' : {
                    'referenceModel' : '$$\\begin{array}{rcl}  y^{\\langle 2\\rangle } & = & v \\\\ \\end{array}$$ ',
                    'referenceVar': '$$v(t)$$ ',
                    ... tIoEq2
                },
            },
            'OrePolynomials': {
                'Example1' : { 
                    'polynomialVar': 's',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    ... tIoEq1 
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... tIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... tIoEq3
                }
            },
            'OreRationals' : {
                'Example1' :{
                    'polynomialVar': 's',
                    'poly1': '$$\\text{OreP}(1,0,y)$$ ',
                    'poly2': '$$\\text{OreP}(u,0)$$ ',
                    'polyq1': 'OreP[1,0]',
                    'polyq2': '$$\\text{OreP}(1,u)$$ ',
                    ... tIoEq1  
                }
            },
            'Realization' :  {
                'Example1' : {
                    'stateVars' : 'x',
                    ... tIoEq1
                },
                'Example2' : {
                    'stateVars' : 'x',
                    ... tIoEq2
                },
                'Example3' : {
                    'stateVars' : 'x',
                    ... tIoEq3
                }
            },
            'Reduction' :  {
                'Example1' : tIoEq1,
                'Example2' : tIoEq2,
                'Example3' : tIoEq3
            },
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... tIoEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... tIoEq2
                }
            },    
            'TransferFunction' : {
                'Example1' : {
                    'polynomialVar': 's',
                    ... tIoEq1
                },
                'Example2' : {
                    'polynomialVar': 's',
                    ... tIoEq2
                },
                'Example3' : {
                    'polynomialVar': 's',
                    ... tIoEq3
                }
            }   
        },
        'SS' : {
            'Accessibility' : {
                'Example1' : tSsEq1,
                'Example2' : tSsEq2
            },
            'Linearization' :  {
                'Example1' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... tSsEq1
                },
                'Example2' : {
                    'newStateVars' : 'z',
                    'newInputVars' : 'v',
                    ... tSsEq2
                }
            },
            'Identifiability' : {
                'Example1' : {
                    'systemEqs' : '$$\\begin{array}{rcl}  \\text{x1}^{\\langle 1\\rangle } & = & \\text{a1} x_2+\\text{a2} \\\\  x_2{}^{\\langle 1\\rangle } & = & u+x_2 \\\\ \\end{array}$$ ',
                    'inputVars' : '$$u(t)$$ ',
                    'timeVar' : '$$\\text{t}$$ ',
                    'parameters' : '{a1, a2}'
                },  
            }, 
            'SequenceH' :  {
                'Example1' : {
                    'subspaceNumber' : 'All',
                    ... tSsEq1
                },
                'Example2' : {
                    'subspaceNumber' : 'All',
                    ... tSsEq2
                }
            },
            'Observability' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq2
                }
            },
            'ObserverForm' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq2
                }
            },  
            'StateSpaceToIO' :  {
                'Example1' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq1
                },
                'Example2' : {
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq2
                }
            },    
            'TransferFunction' :  {
                'Example1' : {
                    'polynomialVar': 's',
                    'outputEqs' : '$$\\begin{array}{rcl}  y & = & x_1 \\\\ \\end{array}$$ ',
                    ... tSsEq1
                }
            }
        }
    }
}