let explanation = {
    "FromIOToOreP" : "Given the set of input-output equations, the function finds two matrices whose elements are polynomials from the non-commutative Ore polynomial ring. With the help of these matrices 'the tangent linearized system description' may be given that relates the differentials of inputs and outputs.",

    "ModelMatching" : "The function solves the model matching problem, either by the feedforward or the feedback compensator.",

    "OrePolynomials" : "This page allows to perform different operations with Ore polynomials.",

    "OreRationals" : "This page allows to perform addition and multiplication of the rational expressions of Ore polynomials.",

    "Realization" : "The function checks whether the system given by the set of input-output equations can be transformed into the classical state-space form and, if possible, finds the state coordinates and the state equations. Otherwise it informs the user that the system is not realizable in the state space form.",

    "Reduction" : "The function checks whether the system, described by the set of input-output equations, is irreducible or not. In case it is irreducible, the functions informs the user about this. In the opposite case the function finds a new, lower order system representation, being transfer equivalent to the original system.",

    "IOSequenceH" : "The function computes the sequence of subspaces of one-forms {Hk} for the system, given by input-output equation(s).",

    "SSSequenceH" : "The function computes the sequence of subspaces of one-forms {Hk} for the system, given by by state-space equations.",

    "IOTransferFunction" : "The function finds the transfer matrix for the system, given by input-output equation(s).",

    "SSTransferFunction" : "The function finds the transfer matrix for the system, given by state-space equations.",

    "Accessibility" : "The function checks whether the system is accessible and if not, then decomposes the system into non-accessible and accessible subsystems.",

    "Linearization" : "The function performs, if possible, static state feedback linearization.",

    "Identifiability" : "THIS DOESN't WORK! The function checks identifiability of the unknown parameters of the system.",

    "Observability" : "The function checks observability of the system and finds observability filtrations.",

    "ObserverForm" : "THIS DOESN't WORK! The function transforms the system, if possible, into the observer form via both the state and output transformations.",

    "StateSpaceToIO" : "The function finds input-output equations starting from state equations.",

    "Submersivity" : "The function checks whether the submersivity property is fulfilled for the system."
}