"use strict";

let result = {
    mathematica:"",
    laTexArg:"",
    laTexNoArg:""
};
let activeButton;


let menuHeaders = document.getElementsByClassName("menuheader");
for (let i = 0; i < menuHeaders.length; i++) {
    menuHeaders[i].addEventListener("click", function() {
    let panel = this.nextElementSibling;
    if (panel.style.display === "block") {
        //closing
        panel.style.display="none";
     } else {
        //opening
        panel.style.display="block";
    }
})
}

function setTitleAndExplanation(title, explanation) {
    document.getElementById('functionTitle').innerText = title;
    document.getElementById('pageExplanation').innerText = explanation;
}



function createPageElements(arr, timeDomain, equationsType, mathFunction){
    activeButton = document.activeElement;
    document.getElementById("infoPanel").style.display = "none";
    document.getElementById("resultPanel").style.display = "none";
    document.getElementById("mainPanel").style.display = "inline-block";

    // set help file name
    let timeDomainNamePart = (timeDomain === 'Shift')? 'discr':'cont';
    let helpFileName = 'add_info/'+ timeDomainNamePart + '_' + mathFunction + '.pdf';
    function openHelp() {
        window.open(helpFileName);
    }
    document.getElementById('theoryButton').onclick = openHelp;
    
    // create data Panel
    let fieldNames = arr;
    

    function smallTableField(id, text, size){
        return `<td>
        <label for="${id}"><p>${text}</p></label>
        <input type="text" id="${id}" size="${size}">
        </td>`;
    }

    let panel = "";
    
    let addFieldIfNeeded = (id, html) => { if (arr.includes(id)) panel += html; }
    let addSmallTableFieldIfNeeded = (id, text, size) => 
        addFieldIfNeeded(id, smallTableField(id, text, size));

    if(mathFunction ==="OrePolynomials") {
        panel += `<table border = "0"><tr>
            <td><label for="poly1">$$p_1=$$</label></td><td><input type="text" id="poly1" size="20"></td>
            <td><label for="poly2">$$p_2=$$</label></td><td><input type="text" id="poly2" size="20"></td>
        </tr></table>`;
    }

    if(mathFunction ==="OreRationals") {
        panel += `<table border = "0"><tr>
            <tr>
                <td rowspan="2">$$\\frac{p_1}{q_1}=$$</td>
                <td id="denominatorBox"><input type="text" id="poly1" size="20"></td>
                <td rowspan="2">$$\\frac{p_2}{q_2}=$$</td>
                <td id="denominatorBox"><input type="text" id="poly2" size="20"></td>
            </tr>
            <tr>
            <td><input type="text" id="polyq1" size="20"></td>
            <td><input type="text" id="polyq2" size="20"></td>
            </tr>
        </tr></table>`;
    }


    let equationsTypeString = (equationsType === 'IO') ? "Input-output equations" : "State-space equations";
    panel += `<div>
            <div>${equationsTypeString}:</div>
            <textarea id="systemEqs" rows="4" cols = "70"></textarea>
        </div>`;

    addFieldIfNeeded("outputEqs", `<div>
        <div>Output equation(s):</div>
        <textarea id="outputEqs" rows="3" cols = "70"></textarea>
    </div>`);

    addFieldIfNeeded("referenceModel", `<div>
        <div>Input-output equation of the reference model:</div>
        <textarea id="referenceModel" rows="4" cols = "70"></textarea>
    </div>`);
    panel += `<table border = "0"><tr>`;
    addSmallTableFieldIfNeeded("inputVars", "Input variable(s)", 15);
    addSmallTableFieldIfNeeded("outputVars", "Output variable(s)", 15);
    addSmallTableFieldIfNeeded("referenceVar", "Reference variable", 15);
    addSmallTableFieldIfNeeded("timeVar", "Time", 4);
    addSmallTableFieldIfNeeded("subspaceNumber", "Number of subspaces", 6,);
    addSmallTableFieldIfNeeded("polynomialVar", "Polynomial variable", 6);
    addSmallTableFieldIfNeeded("stateVars", "State variable(s)", 6);
    addSmallTableFieldIfNeeded("newStateVars", "New state variable(s)", 6);
    addSmallTableFieldIfNeeded("newInputVars", "New input variable(s)", 6);
    addSmallTableFieldIfNeeded("parameters", "Parameter(s)", 6);
    panel += `</tr></table>`;

    if(mathFunction === "OrePolynomials") { panel += `<div id="polynomialOperationDiv">
    <br>
    The operation:
    <table >
        <tr>
            <td><input type="radio" name="oreOp" id="leftDivision" value="LeftQuotientRemainder"><label for="leftDivsion">Left division: \\(p_1 = p_2\\cdot q + r\\)</label></td>
            <td><input type="radio" name="oreOp" id="rightDivision" value="RightQuotientRemainder"><label for="rightDivision">Right division \\(p_1 = q\\cdot p_2 + r\\)</label></td>
            <td><input type="radio" name="oreOp" id="polySum" value="OreAdd"><label for="polySum">\\(p_1 + p_2\\)</label></td>
        </tr>
        <tr>
            <td><input type="radio" name="oreOp" id="leftGCD" value="LeftGCD"><label for="leftGCD">LeftGCD</label></td>
            <td><input type="radio" name="oreOp" id="rightGCD" value="RightGCD"><label for="rightGCD">RightGCD</label></td>
            <td><input type="radio" name="oreOp" id="polyMultiply" value="OreMultiply" checked="checked"><label for="polyMultiply">\\(p_1 \\cdot p_2\\)</label></td>
        </tr>
        <tr>
            <td><input type="radio" name="oreOp" id="leftLCM" value="LeftLCM"><label for="leftLCM">LeftLCM</label></td>
            <td><input type="radio" name="oreOp" id="rightGCD" value="RightGCD"><label for="rightGCD">RightLCM</label></td>
            <td></td>
        </tr>
    </table>
    <br>
    </div>`;
    }

    if(mathFunction === "OreRationals") {panel += `<div id="oreRationalsDiv">
    <br>
    The operation:
    <table>
        <tr>
            <td><input type="radio" name="oreOp" id="oreAdd" value="OreAdd" checked="checked"><label for="oreAdd">Addition: \\(\\frac{p_1}{q_1}+ \\frac{p_2}{q_2}\\)</label></td>
            <td><input type="radio" name="oreOp" id="oreMultiply" value="OreMultiply"><label for="oreMultiply">Multiplication \\(\\frac{p_1}{q_1}\\cdot \\frac{p_2}{q_2}\\)</label></td>
        </tr>

    </table>
    <br>
    </div>`;
    }

    let addCheckBox = (name, description, checkValue) => 
    panel += `<input type="checkbox" id="${name}" ${checkValue}>
    <label for="${name}">${description}</label><br>`;

    if(mathFunction === "ModelMatching"){
        addCheckBox("feedforwardCompensator", "Feedforward compensator", "false");
        addCheckBox("feedbackCompensator", "Feedback compensator", "checked");
    }

    if(mathFunction === "SequenceH") {
        addCheckBox("integrate", "Integrate one-forms?", "false");
    }

    if(mathFunction === "Identifiability") {
        panel += `<input type="radio" name="identifiabilityMethod" id = "identifiabilityAlgebraic" checked="checked">
        <label for="identifiabilityAlgebraic">Algebraic method</label><br>
        <input type="radio" name="identifiabilityMethod" id = identifiabilityLSM>
        <label for="identifiabilityLSM">Least squares method</label><br>`
    }

    if(mathFunction === "Observability"){
        addCheckBox("checkObservability", "Check observability", "checked");
        addCheckBox("observabilityFiltration", "Find observability filtration", "checked");
        addCheckBox("observableSpace", "Find observable space", "checked");
        addCheckBox("unObservableSpace", "Find unobservable space", "checked");
        addCheckBox("observabilityIndices", "Find observability indices", "checked");
    }

    document.getElementById('dataPanel').innerHTML = panel;

    // ======= fill forms =====================================================================

    pasteEquations(timeDomain, equationsType, mathFunction, "Example1"); // takes 1st example for each mathFunction

    // ========== create examples panel ===========================================================
    
    let examplesListContent = "";
    let k=0;
    for (const [key, value] of Object.entries(texExamples[timeDomain][equationsType][mathFunction])){
        examplesListContent += `<div onclick = "pasteEquations('${timeDomain}', '${equationsType}','${mathFunction}', '${key}')">
            <h4>Example ${++k}</h4>
            <p>${value.systemEqs}</p>
            </div>`
    }
    document.getElementById('examplesList').innerHTML = examplesListContent;
    document.getElementById("examplesMenu").classList.toggle("show");
    MathJax.typeset();



    // ========== loob saada() funktsiooni ========================================================
    function saada() {
        activeButton.focus();
        document.getElementById("resultPanel").style.display = "block";
        document.getElementById('tulemus').innerText = "Wolfram Engine computes, wait a moment...";
        let mathExpr = makeMathExpression(fieldNames, timeDomain, equationsType, mathFunction);
        console.log(mathExpr);
        let start = new Date().getTime();
        console.log('sending data to Wolfram Engine...');
        fetch("http://localhost:8000/", {
            method: 'POST',
            body: mathExpr
        })
        .then(vastus => vastus.text())
        .then(tekst => {
            console.log(tekst);
            let splittedText = tekst.split("NLControl string break");
            result.mathematica = splittedText[0];            // 1. on tulemus Mathematica avaldisena
            result.laTexArg = splittedText[2];               // 2. on tulemus Latex-is ilma argumendita t
            result.laTexNoArg = splittedText[1];             // 3. on tulemus Latex-is koos argumendiga t
            refreshResult();
            let end = new Date().getTime();
            console.log("aega kulus: "+(end - start));
        });
    }
    document.getElementById('computeButton').onclick = saada;
}

function pasteEquations(timeDomain, equationsType, mathFunction, exId) {
    activeButton.focus();
    let exmp = mathExamples[timeDomain][equationsType][mathFunction][exId];
    //if(document.getElementById("inputMathematica").checked){
    //    exmp = mathExamples[timeDomain][equationsType][mathFunction][exId];
    //} else {
    //    exmp = texExamples[timeDomain][equationsType][mathFunction][exId];
    //}
    console.log(exmp);
    for (const [key, value] of Object.entries( exmp) ){
        document.getElementById(key).value = value 
    }
}

function getRadioValue(radioName) {
    let result;
    for(const radio of document.getElementsByName(radioName) ){
        if(radio.checked) {result = radio.value;}
    }
    return result;
}

function clearInputFields() {
    for(const field of document.getElementsByTagName("input")){
        if(field.type === "text") { field.value = "";}
    }
    for(const field of document.getElementsByTagName("texarea")){
        field.value = "";
    }
}

// ============= control functions =====================================================

function FromIOToOreP(timeDomain){
    setTitleAndExplanation('IO equations → Ore polynomials', explanation.FromIOToOreP);
    createPageElements(["inputVars", "outputVars", "timeVar", "polynomialVar"], timeDomain, 'IO', 'FromIOToOreP'); 
}

function ModelMatching(timeDomain){
    setTitleAndExplanation('Model Matching', explanation.ModelMatching);
    createPageElements(["referenceModel", "referenceVar", "inputVars", "outputVars", "timeVar"], 
        timeDomain, 'IO', 'ModelMatching'); 
}

function OrePolynomials(timeDomain){
    setTitleAndExplanation('Ore Polynomials', explanation.OrePolynomials);
    createPageElements(["poly1", "poly2", "polynomialVar", "inputVars", "outputVars", "timeVar"], timeDomain, 'IO', 'OrePolynomials');
}

function OreRationals(timeDomain){
    setTitleAndExplanation('Rational expressions of Ore polynomials', explanation.OreRationals);
    createPageElements(["poly1", "poly2", "polyq1", "polyq2", "polynomialVar", "inputVars", "outputVars", "timeVar"], timeDomain, 'IO', 'OreRationals');
}

function Realization(timeDomain) {
    setTitleAndExplanation('Realization', explanation.Realization);
    createPageElements(["inputVars", "outputVars", "timeVar", "stateVars"], timeDomain, 'IO', 'Realization');
}

function Reduction(timeDomain) {
    setTitleAndExplanation('Reduction', explanation.Reduction);
    createPageElements(["inputVars", "outputVars", "timeVar"], timeDomain, 'IO', 'Reduction');
}

function IOSequenceH(timeDomain) {
    setTitleAndExplanation('Sequence \\(\\{\\mathcal{H}_k\\}\\)', explanation.IOSequenceH);
    createPageElements(["inputVars", "outputVars", "timeVar", "subspaceNumber"], timeDomain, 'IO', 'SequenceH');
}

function IOTransferFunction(timeDomain) {
    setTitleAndExplanation('Transfer Function', explanation.IOTransferFunction);
    createPageElements(["inputVars", "outputVars", "timeVar", "polynomialVar"], timeDomain, 'IO', 'TransferFunction');
}

/* ------------------ state space ----------------------------------------- */

function Accessibility(timeDomain){
    setTitleAndExplanation('Accessibility', explanation.Accessibility);
    createPageElements(["inputVars", "timeVar"], timeDomain, 'SS', 'Accessibility');
}

function Linearization(timeDomain) {
    setTitleAndExplanation('Linearization', explanation.Linearization);
    createPageElements(["inputVars", "timeVar", "newStateVars", "newInputVars"], timeDomain, 'SS', 'Linearization');
}

function Identifiability(timeDomain) {
    setTitleAndExplanation('Identifiability', explanation.Identifiability);
    createPageElements(["inputVars", "timeVar", "parameters"], timeDomain, 'SS', 'Identifiability');
}

function Observability(timeDomain) {
    setTitleAndExplanation('Observability', explanation.Observability);
    createPageElements(["outputEqs", "inputVars", "timeVar"], timeDomain, 'SS', 'Observability');
}

function ObserverForm(timeDomain) {
    setTitleAndExplanation('Observer form', explanation.ObserverForm);
    createPageElements(["outputEqs", "inputVars", "timeVar"], timeDomain, 'SS', 'ObserverForm');
}

function SSSequenceH(timeDomain) {
    setTitleAndExplanation('Sequence \\(\\{\\mathcal{H}_k\\}\\)', explanation.SSSequenceH);
    createPageElements(["inputVars", "timeVar", "subspaceNumber"], timeDomain, 'SS', 'SequenceH');
}

function SSTransferFunction(timeDomain) {
    setTitleAndExplanation('Transfer function', explanation.SSTransferFunction);
    createPageElements(["outputEqs", "inputVars", "timeVar", "polynomialVar"], timeDomain, 'SS', 'TransferFunction');
}

function Submersivity(timeDomain){
    setTitleAndExplanation('Submersivity', explanation.Submersivity);
    createPageElements(["inputVars", "timeVar"], timeDomain, 'SS', 'Submersivity'); 
}

function StateSpaceToIO(timeDomain){
    setTitleAndExplanation('State-Space → I/O', explanation.StateSpaceToIO);
    createPageElements(["outputEqs", "inputVars", "timeVar"], timeDomain, 'SS', 'StateSpaceToIO'); 
}

/* ==================================================================================*/
/* -------------------------- makeMathExpression ------------------------------------*/

/* teeb väljade sisu ja globaalsete muutujate põhjal valmis Mathematica avaldise, mis saadetakse Wolfram Enginele*/
function makeMathExpression(arr, timeDomain, equationsType, mathFunction){
    // Paneme vaikeväärtused.
    let values = {
        systemEqs:null,
        outputEqs:"{}",
        inputVars:"",
        outputVars:"", 
        timeVar:"", 
        polynomialVar:"",
        stateVars:"",
        newStateVars:"",
        subspaceNumber:"All",
        newInputVars:"",
        parameters:"",
        referenceModel:"{}",
        referenceVar:"",
        poly1:"",
        poly2:"",
        polyq1:"",
        polyq2:""
    }
   
    /* Mõned väljad on kõigi funktsioonide korral alati olemas */
    let newArr = ["systemEqs", "inputVars", "timeVar"].concat(arr);
    /* Küsin kõiki vajalikke asju. */
    for (const key of newArr) {
        values[key] = document.getElementById(key).value;
    }

    /*Kui LateXi sisendi nupuke on valitud, siis paneb andmetel ümber Mathematica funktsiooni, mis teisendab LateXist Mathematica avaldiseks
    if(document.getElementById("inputLaTeX").checked){
        for (const key of newArr) {
            values[key] = `ToNLCExpression["\\{${values[key]}\\}", TeXForm]`;
        }
    }*/

    /* teeb valmis NLControli võrrandite objekti*/
    let mathExpr;
    if(equationsType === "IO") {
        mathExpr = `sys = ModelToRules @ IO[${values.systemEqs}, ${values.inputVars}, ${values.outputVars}, ${values.timeVar}, ${timeDomain}]; `;
    } else {
        mathExpr = `sys = StateEquations[${values.systemEqs}, ${values.inputVars}, ${values.timeVar}, ${values.outputEqs}, ${timeDomain}]; `;
    }

    /*teeb valmis koodi, mis kutsub välja vajaliku Mathematica funktsiooni*/
    let oreOperation;
    switch(mathFunction) {
        case "FromIOToOreP":
            mathExpr += `FromIOToOrePGUI[sys, ${values.polynomialVar}]`; break;
        case "ModelMatching":
            let ffComp = (document.getElementById("feedforwardCompensator").checked)? "True": "False";
            let fbComp = (document.getElementById("feedbackCompensator").checked)? "True": "False";
            mathExpr += `refeq = ModelToRules @ IO[${values.referenceModel}, ${values.referenceVar}, 
                ${values.outputVars}, ${values.timeVar}, ${timeDomain}]; 
                ModelMatchingGUI[sys, refeq, ${ffComp}, ${fbComp}]`; break;
        case "OrePolynomials":
            oreOperation = getRadioValue("oreOp");
            mathExpr += `OrePolynomialsGUI[${oreOperation}, ${values.poly1}, ${values.poly2}, ${values.polynomialVar}, sys]`; break;
        case "OreRationals":
            oreOperation = getRadioValue("oreOp");
            mathExpr += `OrePolynomialsGUI[${oreOperation}, OreR[${values.polyq1}, ${values.poly1}], 
            OreR[${values.polyq2}, ${values.poly2}], ${values.polynomialVar}, sys]`; break;
        case "Realization": 
            mathExpr += `RealizationGUI[sys, ${values.stateVars}]`; break;
        case "Reduction":
            mathExpr += `ReductionGUI[sys]`; break;
        case "SequenceH":
            let integr = (document.getElementById("integrate").checked)? "True": "False";
            mathExpr += `SequenceHGUI[sys, ${values.subspaceNumber}, ${integr}]`; break;
        case "TransferFunction":
            mathExpr += `TransferFunctionGUI[${values.polynomialVar}, sys]`; break;
        case "Accessibility":
            mathExpr += `AccessibilityGUI[sys]`; break;
        case "Linearization":
            mathExpr += `LinearizationGUI[sys, ${values.newStateVars}, ${values.newInputVars}]`; break;
        case "Identifiability":
            let met = (document.getElementById("identifiabilityAlgebraic").checked)?"Algebraic":"LSM";
            mathExpr += `IdentifiabilityGUI[sys, ${values.parameters}, Method->${met}]`; break;
        case "Observability":
            let observabilityCheckBoxes = ["checkObservability", "observabilityFiltration", "observableSpace", 
            "unObservableSpace","observabilityIndices"];
            let idx = [];
            observabilityCheckBoxes.forEach(box => {
                if(document.getElementById(box).checked) {idx.push("True")} else {idx.push("False")}
            });
            mathExpr += `ObservabilityGUI[sys, {${idx}}]`; break;
        case "ObserverForm":
            mathExpr += `ObserverFormGUI[sys]`; break;
        case "Submersivity":
            mathExpr += `SubmersivityGUI[sys]`; break;
        case "StateSpaceToIO":
            mathExpr += `StateSpaceToIOGUI[sys]`; break;
        default:
            console.error(`Internal error mahtFunction=${mathFunction}`);
    }
/*mathExpr2 on see avaldis, mille Mathematica saadab tagasi veebilehele. 
sys - lähtevõrrandid, result - tulemus, ülesande vastus 
Mõlemad on kolmel erineval kujul:
1) Mathematica avaldisena, 
2) LateXis ilma argumentdita t 
3) latexis koos argumendiga t 
*/
  /*  let mathExpr2 = ` {
        sys, 
        result, 
        NLCToTeX[BookForm[sys, TimeArgument -> False]],
        NLCToTeX[GuiForm["${mathFunction}", result, sys, TimeArgument -> False]],
        NLCToTeX[BookForm[sys]],
        NLCToTeX[GuiForm["${mathFunction}", result, sys]]
    }`;
    return mathExpr + mathExpr2;*/
    
    return mathExpr;
}

/* Kuvab tulemuse õiges formaadis (st kas latex formaaditud, latexi kood või mathematica. Argument t kas kuvatakse või ei.)*/
function refreshResult() {
    let resultField = document.getElementById('tulemus');
    if(!document.getElementById("outputLaTeX").checked ) {
        MathJax.typesetClear();
    }
    if(document.getElementById("outputMathematica").checked){
        resultField.innerText = result.mathematica;
    }
    else if( document.getElementById("timeArgumentFalse").checked ){
        resultField.innerText = result.laTexNoArg
    } else {
        resultField.innerText = result.laTexArg
    }
    if(document.getElementById("outputLaTeX").checked ) {
        MathJax.typeset();
    }   
}



