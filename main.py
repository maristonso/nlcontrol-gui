import http.server
import socketserver
import subprocess
import traceback

PORT = 8000
WOLFRAMSCRIPT_TIMEOUT = 30 

OK = 200
INTERNAL_SERVER_ERROR = 500  #This python server is broken
BAD_GATEWAY = 502  #Wolframscript exited with non zero exit code
GATEWAY_TIMEOUT = 504  #Wolframscript took too long.


# Handler on klass, mis pärib klassi SimpleHTTPRequestHandler omadused
class Handler(http.server.SimpleHTTPRequestHandler): 
    def my_read_data(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        return self.rfile.read(content_length) # <--- Gets the data itself
    def my_send_data(self, code, bytes):
        self.send_response(code)
        self.send_header("Content-type", "text/text")
        self.end_headers()
        self.wfile.write(bytes)
    def do_POST(self):
        try:
            self.actual_work()
        except Exception as e:
            text = f'''NLC21 ERROR: Internal python server error.
            {e}
            {traceback.format_exc()}
            '''
            print(text)
            self.my_send_data(INTERNAL_SERVER_ERROR, text.encode('utf-8'))

    def actual_work(self):
        input_bytes = self.my_read_data()
        print(f'NLC21 REQUEST: data is {input_bytes}')
        try:
            completed = subprocess.run(
                ['wolframscript', 'main.wls'], 
                input=input_bytes, 
                capture_output=True,
                check=True,
                timeout=WOLFRAMSCRIPT_TIMEOUT
                )
        except subprocess.CalledProcessError as e:
            text = f'''NLC21 ERROR: wolframscript exited with a non-zero exit code ({e.returncode}).
            stdout = {e.stdout}
            stderr = {e.stderr}
            '''
            print(text)
            self.my_send_data(BAD_GATEWAY, text.encode('utf-8'))
        except subprocess.TimeoutExpired as e:
            text = f'''NLC21 ERROR: wolframscript ran for too long.
            stdout = {e.stdout}
            stderr = {e.stderr}
            '''
            print(text)
            self.my_send_data(GATEWAY_TIMEOUT, text.encode('utf-8'))
        else:
            text = f'''NLC21 SUCCESS: Got a response from wolframscript
            stdout = {completed.stdout}
            stderr = {completed.stderr}
            '''
            print(text)
            self.my_send_data(OK, completed.stdout)


with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("NLC21 serving at port", PORT)
    httpd.serve_forever()
